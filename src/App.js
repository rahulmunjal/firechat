import Button from './components/Button';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';



firebase.initializeApp({

 
    apiKey: "AIzaSyAt8KlSjVzKAsBdIIIh68yEQ6kmm2Q6pm8",
    authDomain: "react-firechat-d3569.firebaseapp.com",
    projectId: "react-firechat-d3569",
    storageBucket: "react-firechat-d3569.appspot.com",
    messagingSenderId: "65196190150",
    appId: "1:65196190150:web:e379c14a48b808a2ae2ac7"
  
});


function App() {

  const [user, setUser] = useState(() => auth.currentUser);

  userEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(user => {
      if (user ){
        setUser(user);
      } 
      else {
        setUser(null);
      }
    });
    return unsubscribe;
  }, []);
  const signInWithGoogle = async( () => auth.currentUser);
  auth.useDeviceLanguage();

};

if (initializing) return 'Loading...';


  return (
    <div>
      {
        user ? (
          'Welcome to the Chat'
        ):(
          <Button onClick={signInWithGoogle}>Sign in with google</Button>
        )
      }
    </div>
  );
}

export default App;
